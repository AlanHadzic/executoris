### **ExecutorIS**

*Author: Alan Hadžić*

ExecutorIS reads the input file with jobs configurations and execute the job every N-seconds according to the job configuration.

Structure of input file: 

``` 
 <jobId>; <timeout>; <jobContent>

 <jobId> - ID-job
 <timeout> - time delay before executing job
 <jobContent> - output message, that must be printed when the job is executed
```

Output: 

```

<jobDateTime> - <jobId> - <jobContent> 

```

***Input file*** is passed as parameter.




---

## Requirements

To compile and run ExecutorIS you must have installed:

1. Java JDK 8 or higher 
2. Git versioning system (optional)
3. IntelliJ IDEA (optional)

---

## Run with IntelliJ IDEA

1. Clone source code from BitBucket:
    ``` 
   git clone git@bitbucket.org:AlanHadzic/executoris.git  
   ```
2. In IntelliJ IDEA go to: File - Open - <search_for_project>. Import project "executoris"
3. In IntelliJ IDEA go to: Run - Edit Configuration. Update "Program Arguments" to reference the absolute path to your input file. 
4. Run 

---

## Create JAR and run JAR

1. With cmd or terminal navigate to ``` .../executoris/src ``` directory.
2. Compile our code:
    ```
    javac Executor.java
    ```
3. Create JAR file, with manifest file
    ```
   jar cfm Executor.jar manifest.txt *.class
   ```
4. Run JAR:
    ```
   java -jar Executor.jar [absolute_path_to_input_file]
   ```

---

## Advantages - Disadvantages

Calculating modulo ```(currentTime - startTime) % timeout == 0```

Advantages:

* more simple logic 
* more readable code.

Disadvantages:

* over the time difference ```(currentTime - startTime)``` can be huge and calculating modulo will not be optimal.

Improvements:

Calculating modulo only on first iteration and initialize "nextExecutionTime" inside JobDefinitionModel.
nextExecutionTime is calculated: ```nextExecutionTime = currentTime + timeout```.
On every other iterations instead calculating all modules we will compare if ```currentTime >= nextExecutionTime``` if this statement is true execute job print output and update nextExecutionTime.
 

---

## Thanks

I would like to thank you for taking your time and reviewing ExecutorIS project.

---
   
## License

Copyright 2020 AlanH 

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

