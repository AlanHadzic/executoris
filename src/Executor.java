import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Executor {

    public static void main(String[] args) {

        HashMap<Long, JobDefinitionModel> jobsHashMap = new HashMap<>();

        try {
            readFileToHashMap(jobsHashMap, args[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        jobScheduler(jobsHashMap, LocalDateTime.now());
    }

    private static void readFileToHashMap(HashMap<Long, JobDefinitionModel> jobsHashMap, String file_path) throws IOException {

        File inputFile = new File(file_path);
        if(!inputFile.exists() || inputFile.isDirectory()) throw new IOException("File not found.");

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        String line = reader.readLine();
        long jobId; int jobTimeout; String jobContent;

        while(line != null) {

            String[] splitLine = line.split(";");

            jobId = Long.parseLong(splitLine[0].trim());
            jobTimeout = Integer.parseInt(splitLine[1].trim());
            jobContent = splitLine[2];

            jobsHashMap.put(jobId, new JobDefinitionModel(jobTimeout, jobContent));

            line = reader.readLine();
        }
        reader.close();
    }

    private static void jobScheduler(HashMap<Long, JobDefinitionModel> jobsDefinitions, LocalDateTime startTime) {

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(
                new PeriodicTaskRunnable(jobsDefinitions, startTime),
                0,
                1,
                TimeUnit.SECONDS);

    }

    static class PeriodicTaskRunnable implements Runnable {

        private final HashMap<Long, JobDefinitionModel> jobsDefinitions;
        private final LocalDateTime startTime;

        public PeriodicTaskRunnable(HashMap<Long, JobDefinitionModel> jobsDefinitions, LocalDateTime startTime) {
            this.jobsDefinitions = jobsDefinitions;
            this.startTime = startTime;
        }

        @Override
        public void run() {
            LocalDateTime currentTime = LocalDateTime.now();
            int secondsSinceStart = (int) startTime.until(currentTime, ChronoUnit.SECONDS);
            int jobTimeOut; String jobContent;

            for(Long jobId : jobsDefinitions.keySet()) {

                jobTimeOut = jobsDefinitions.get(jobId).getTimeout();
                jobContent = jobsDefinitions.get(jobId).getJobContent();

                if(secondsSinceStart != 0 && secondsSinceStart % jobTimeOut == 0) {
                    // TODO here execute or make event to execute job
                    printResult(jobId, jobContent);
                }
            }
        }

        private void printResult(Long jobId, String jobContent) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            System.out.println(String.format("%s - %s - %s",
                    dateTimeFormatter.format(LocalDateTime.now()),
                    jobId,
                    jobContent));
        }
    }

    static class JobDefinitionModel {

        private final int timeout;
        private final String jobContent;

        JobDefinitionModel(int timeout, String jobContent) {
            this.timeout = timeout;
            this.jobContent = jobContent;
        }

        public String getJobContent() {
            return jobContent;
        }

        public int getTimeout() {
            return timeout;
        }

    }

}
